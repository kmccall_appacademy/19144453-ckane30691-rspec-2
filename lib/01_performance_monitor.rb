def measure(num = 1)
  t = Time.now
  count = 0
  for idx in (1..num)
    yield
    count += 1
  end
  return ((t - Time.now).abs / count)
end