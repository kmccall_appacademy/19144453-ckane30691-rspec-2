def reverse_word(word)
  return word.reverse
end

def reverser
  result = []
  for word in yield.split(" ")
    result << reverse_word(word)
  end
  result.join(" ")
end

def adder(num = 1)
  return yield + num
end

def repeater(times_to_execute = 1)
  for idx in (1..times_to_execute)
    yield
  end
end